import Vue from 'vue'
import Router from 'vue-router'

import Login from '@/components/Login'
import HomeUser1 from '@/pages/HomeUser1'
import HomeUser2 from '@/pages/HomeUser2'
import HomeUser3 from '@/pages/HomeUser3'

import SeeMovie1 from '@/pages/SeeMovie1'
import SeeMovie2 from '@/pages/SeeMovie2'
import SeeMovie3 from '@/pages/SeeMovie3'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Login',
      component: Login
    },
    {
      path: '/user/1',
      component: HomeUser1
    },
    {
      path: '/user/2',
      component: HomeUser2
    },
    {
      path: '/user/3',
      component: HomeUser3
    },
    {
      path: '/login',
      redirect: '/'
    },
    {
      path: '/user/1/see-movie',
      component: SeeMovie1
    },
    {
      path: '/user/2/see-movie',
      component: SeeMovie2
    },
    {
      path: '/user/3/see-movie',
      component: SeeMovie3
    }
  ]
})
